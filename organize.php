<?php
/**
 * organize.php - use image exif data to organize image files
 * first paramter is the target output directory
 * all remaining arguments are treated as directories to scan
 */
date_default_timezone_set('America/Los_Angeles');
try {
    if (!isset($argv[1])) {
        throw new Exception('Missing target directory');
    }
    if (!isset($argv[2])) {
        throw new Exception('Missing directory to scan');
    }
    $if = new ImageFiler($argv[1]);
    $if->addDir($argv[2]);
    $args = count($argv);
    if ($args > 3) {
        for ($i=3; $i<$args; $i++) {
            $if->addDir($argv[$i]);
        }
    }
    $if->run();
} catch (Exception $e) {
    echo $e->getMessage()."\n";
    exit(1);
}
class ImageFiler {

    /**
     * directories to scan
     * @var array
     */
    protected $_dirs   = array();
    /**
     * target directory where files will be moved to
     * @var str
     */
    protected $_target = '';
    /**
     * initialize image filer
     * @param str $outputDir
     * @throws Exception
     */
    public function __construct($outputDir)
    {
        if (!is_dir($outputDir) && !mkdir($outputDir, 0775)) {
            throw new Exception('Target Path Could not be created:'. $outputDir);
        }
        if (!function_exists('exif_read_data')) {
            throw new Exception('Exif php support is required');
        }
        $this->_target = realpath($outputDir);
    } 
    /**
     * add directory to scan
     * @param unknown $dir
     * @throws Exception
     * @return ImageFiler
     */
    public function addDir($dir)
    {
        if (!is_dir($dir) || !is_readable($dir)) {
            throw new Exception('Cannot Read Dir: '.$dir);
        }
        
        if (!in_array($dir, $this->_dirs)) {
            $this->_dirs[] = $dir;
        }
        return $this;
    }
    /**
     * scan directories for image files
     */
    public function run()
    {
        foreach ($this->_dirs as $dir) {
            $files = $this->_scanDir($dir);
            foreach ($files as $file) {
                $this->_processFile($file);
            }
        }
    }
    /**
     * process file for exif data
     * @param str $file
     */
    protected function _processFile($file)
    {
        if (strpos($file, '.DS_Store') !== false) {
            return;
        }
        $ext      = strtolower(substr($file, -3));
        $mtime    = filemtime($file);
        $usemtime = false;
        if (in_array($ext, ['jpg', 'png'])) {
             $exifData = exif_read_data($file);
             if (!is_array($exifData) || (!isset($exifData['DateTimeOriginal']) && !isset($exifData['FileDateTime']))) {
                 $usemtime = true;
             }
        } else {
            $usemtime = true;
        }
        if ($usemtime) {//use file date info instead @todo make confifgurable
            $exifData = ['FileDateTime' => $mtime];
        } 
        if (isset($exifData['DateTimeOriginal'])) {
            $shotDate = $exifData['DateTimeOriginal'];
            $time     = strtotime($shotDate);
        } else {//fall back to FileDateTime...
            $time     = $exifData['FileDateTime'];
        }
        $format   = date('Y/n/Y_m_d', $time);
        $dir      = $this->_target.'/'.$format;
        $name     = basename($file);
        if (!is_dir($dir)) {
            mkdir($dir, 0775, true);
        }
        $newPath  = $dir.'/'.$name;
        if (realpath($newPath) == realpath($file)) {
            return;
        }
        if (is_file($newPath)) {
            echo 'File Already Exists: '.$newPath."\n";
            $md5Old = md5_file($file);
            $md5New = md5_file($newPath);
            if ($md5New === $md5Old) {
                echo "Exact duplicate ({$md5New} == {$md5Old}) Removing...{$file}\n\n";
                //fwrite(STDOUT, "DELETE FILE [$file]: "); // Output - prompt user
                //$answer= fgets(STDIN);
                //if (strtolower(trim($answer)) == 'yes') {
                    unlink($file);
                //}
            }
        } else {
            rename($file, $newPath);
        }
    }
    /**
     * get directory files
     *  default behavior is to recursively collect file names (no directories) 
     * @param str $path
     * @param bool $includeDirs
     * @param int $maxDepth
     * @return array 
     */
    protected function _scanDir($path, $includeDirs = false, $maxDepth = null)
    {
        $files  = array();
        $d      = dir($path);
        static $depth = 1;
        while (false !== ($entry = $d->read())) {
            if ($entry == '.' || $entry == '..') {
                continue;
            }
            $fullPath = realpath($path.'/'.$entry);
            if (is_dir($fullPath)) {
                $depth++;
                if ($maxDepth == null || $depth <= $maxDepth) {
                    $subFiles    = $this->_scanDir($fullPath, $includeDirs, $maxDepth);
                    $files       = array_merge($files, $subFiles);
                }
                $depth--;
                if ($includeDirs) {
                    $files[]    = $fullPath;
                }
            } else {
                $files[]     = $fullPath;
            }
        }
        $d->close();
        return $files;
    }
}
